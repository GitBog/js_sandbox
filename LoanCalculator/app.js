// Main loan calculator app js file
// Listen for submit event
(function getEvents(){
    document.getElementById('loan-form').addEventListener('submit', function(e){
        // Hide results div
        document.getElementById('results').style.display = 'none';
        // Show loader
        document.getElementById('loading').style.display = 'block';

        setTimeout(calculateResults, 2000);
        // Prevent submit default behaviour
        e.preventDefault();
    });
}());

// Call main calculate method
function calculateResults()
{
    // For debugging purposes - console.log('Submitted!');
    // Make calculations
    const principal = getFormValues('amount');
    const calculatedInterest = getFormValues('interest') / 100 / 12;
    const calculatedPayments = getFormValues('years') * 12;
    // Calculate monthly payments
    const x = Math.pow(1 + calculatedInterest, calculatedPayments);
    const monthly = (principal * x * calculatedInterest)/(x-1);
    if(isFinite(monthly))
    {
        getFormResults('monthly-payment').value = monthly.toFixed(2);
        getFormResults('total-payment').value = (monthly * calculatedPayments).toFixed(2);
        getFormResults('total-interest').value = ((monthly * calculatedPayments) - principal).toFixed(2);
        // Show results div
        document.getElementById('results').style.display = 'block';
        document.getElementById('loading').style.display = 'none';
    }else{
        getError('Please check your numbers');
    }
}

// Get form values
function getFormValues(formVal)
{
    return parseFloat(document.getElementById(formVal).value);
}

// Get form results
function getFormResults(formRes)
{
    return document.getElementById(formRes);
}

// Get any errors

function getError(error)
{
    // Hide results and error
    document.getElementById('results').style.display = 'none';
    document.getElementById('loading').style.display = 'none';
    // Create error div
    const errorDiv = document.createElement('div');
    // Add class name
    errorDiv.className = 'alert alert-danger';
    // Create text node and append to div
    errorDiv.appendChild(document.createTextNode(error));
    // Get UI parent and sibling
    const card = document.querySelector('.card');
    const heading = document.querySelector('.heading');
    // Insert error above heading
    card.insertBefore(errorDiv, heading);
    // Clear error after 3 seconds
    setTimeout(clearError, 3000);
}

// Clear error
function clearError()
{
    document.querySelector('.alert').remove();
}
