// Main task list app js file
//Define UI variables

const form = document.querySelector('#task-form');
const taskInput = document.querySelector('#task');
const taskList = document.querySelector('.collection');
const filter = document.querySelector('#filter');
const clearBtn = document.querySelector('.clear-tasks');

// Load event listeners
(function loadEventListeners()
{
    // DOM load event
    document.addEventListener('DOMContentLoaded', getLocalStorage);
    // Add task event
    form.addEventListener('submit', addTask);
    // Delete task event
    taskList.addEventListener('click', deleteTask);
    // Clear task event
    clearBtn.addEventListener('click', clearTasks);
    // Filter tasks
    filter.addEventListener('keyup', filterTasks);
}());

// Add task method
function addTask(e)
{
    // Basic validation
    if(taskInput.value === '' || taskInput.value === null){
        getMessage(document.querySelector('#message'), 'error', 'Please fill in a task!');
    }else{
    // Create new li element
    const li = document.createElement('li');
    li.className = 'collection-item';
    li.appendChild(document.createTextNode(taskInput.value));

    // Create new a:href element
    const a = document.createElement('a');
    a.className = 'delete-item secondary-content';
    a.innerHTML = '<i class="fa fa-remove"></i>';

    // Append a to li
    li.appendChild(a);

    // Append li to ul
    taskList.appendChild(li);

    // Optional - store to LocalStorage api
    persistToLocalStorage(taskInput.value);

    // Clear user input
    taskInput.value = '';
    // Success message
    getMessage(document.querySelector('#message'), 'success', 'Task added!');
    }
    // Prevent default submit event behaviour
    e.preventDefault();
}

// Get localstorage tasks
function getLocalStorage()
{
    let tasks;

    if(localStorage.getItem('tasks')===null){
        tasks = [];
    }else{
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.forEach(function(task){
        // Create new li element
        const li = document.createElement('li');
        li.className = 'collection-item';
        li.appendChild(document.createTextNode(task));

        // Create new a:href element
        const a = document.createElement('a');
        a.className = 'delete-item secondary-content';
        a.innerHTML = '<i class="fa fa-remove"></i>';

        // Append a to li
        li.appendChild(a);

        // Append li to ul
        taskList.appendChild(li);
    });
}

// Persist to local storage
function persistToLocalStorage(task)
{
    let tasks;

    if(localStorage.getItem('tasks')===null){
        tasks = [];
    }else{
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(tasks));
}


// deleteTask method
function deleteTask(event)
{
    if(event.target.className=='fa fa-remove'){
        if(confirm("Are you sure you want to delete?")){
            event.target.parentElement.parentElement.remove();
            // Remove from localStorage
            removeFromLocalStorage(event.target.parentElement.parentElement);
        }
    }
}

// Remove from local storage
function removeFromLocalStorage(task)
{
    let tasks;

    if(localStorage.getItem('tasks')===null){
        tasks = [];
    }else{
        tasks = JSON.parse(localStorage.getItem('tasks'));
    }

    tasks.forEach(function(taskItem, index){
       if(task.textContent === taskItem){
           tasks.splice(index, 1);
       }

    });
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

// clearTask method
function clearTasks()
{
    if(confirm("Are you sure you want to clear all the tasks?")){
        taskList.innerHTML='';
    }
    // Clear tasks from local storage
    clearTasksFromLocalStorage();
}

// Clear tasks from local storage
function clearTasksFromLocalStorage()
{
    localStorage.clear();
}

// filterTasks method

function filterTasks(event)
{
    let filter_value = event.target.value.toLowerCase();

    document.querySelectorAll('.collection-item').forEach(function(task){
        const item = task.firstChild.textContent.toLowerCase();
        if(item.indexOf(filter_value) != -1){
            task.style.display = 'block';
        }else{
            task.style.display = 'none';
        }
    });
    //console.log('Key pressed!');
}

// getMessage method
function getMessage(selector, class_name, message)
{
    selector.style.display='block';
    selector.className = class_name;
    selector.textContent = message;
    // Make the message go away after 3 miliseconds
    setInterval(function(){
        selector.style.display='none';
    },2500);
}



