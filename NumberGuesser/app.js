// Number guesser app js file

/**
Game rules:
- Player must games a number between a min and a max
- Player gets a certain amount of guesses
- Notify player of guesses remaining
 - Notify player of the correct answer if lose
 - Let player choose to play again
 */

// Set options
let min = 1,
    max = 10,
    winningNumber = getRandomNumber(min, max),
    guessesLeft = 3;

// Set UI elements
const game = document.querySelector('#game'),
      minNum = document.querySelector('.min-num'),
      maxNum = document.querySelector('.max-num'),
      guessBtn = document.querySelector('#guess-btn'),
      guessInput = document.querySelector('#guess-input'),
      message = document.querySelector('.message');

// Assign min and max
minNum.textContent = min;
maxNum.textContent = max;


// Load page events
(function loadEvents()
{
    guessBtn.addEventListener('click', startGame);
}());


function startGame(e)
{
    let userGuess = parseInt(guessInput.value);
    // Validate user input
    if(isValid(userGuess))
    {
        // Call game logic
        gameBusinessLogic(userGuess);
    }


    e.preventDefault();
}

function isValid(inputValue)
{
    if(isNaN(inputValue) || inputValue < min || inputValue > max)
    {
        setMessage(`Please enter a number between ${min} and ${max}`, 'text-danger');
        return false;
    }
    return true;
}

function gameBusinessLogic(guess)
{
    if(guess === winningNumber)
    {
        // If the player won call gameOver!
        gameOver(true, `${winningNumber} is correct, YOU WIN!`, 'text-success');
    }else{
        guessesLeft-=1;
        if(guessesLeft === 0)
        {
            gameOver(false, `Game over, YOU LOST! The correct number was ${winningNumber}`, 'text-danger');
        }else{
            // Keep trying if you didnt guess the correct number
            guessInput.value = '';
            guessInput.style.borderColor = 'red';
            setMessage(`${guess} is not correct, you have ${guessesLeft} guesses left`, 'text-danger');
        }
    }
}

function gameOver(won, message)
{
    let color;
    won === true ? color = 'green': color = 'red';
    guessInput.setAttribute('disabled', true);
    guessInput.style.borderColor = color;
    setMessage(message);
    guessBtn.textContent = 'PLAY AGAIN!';
    guessBtn.addEventListener('click', function(){
       window.location = 'index.html';
    });
}

function setMessage(msg, textClass)
{
    message.className = textClass;
    message.textContent = msg;
}

function getRandomNumber(min, max)
{
   return Math.floor(Math.random() * max + min);
}

// Helper functions
function getYear()
{
    let date = new Date();
    const year = date.getFullYear();

    const footer = document.querySelector('footer');
    const span = document.createElement('span');

    span.appendChild(document.createTextNode(' ' + year));
    return footer.appendChild(span);

}